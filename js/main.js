// Constants
var financeWebserviceUrl = "https://api.dukapc.dk/FinanceWebservice/";
// var financeWebserviceUrl = "http://localhost:18754/";

// Gets query params
var QueryString = function () {
  // This function is anonymous, is executed immediately and
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = decodeURIComponent(pair[1]);
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
      query_string[pair[0]] = arr;
        // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]));
    }
  }
    return query_string;
}();

// Constants
var DEFAULT_VIEW = 1, LOADING_VIEW = 2, LOGIN_VIEW = 3, CREDIT_CARD_INFO_VIEW = 4, UPDATE_CARD_VIEW = 5, NO_CREDIT_CARD_VIEW = 6,
    ADD_CREDIT_CARD_VIEW = 7, NO_SUBSCRIPTION_VIEW = 8, CREATE_SUBSCRIPTION_VIEW = 9, UPDATE_EXPIRED_CARD_VIEW = 10, UPDATE_REJECTED_CARD_VIEW = 11,
    AUTOMATIC_CREDIT_CARD_PAYMENT_NOT_ENABLED_VIEW = 12, SUBSCRIPTION_CANCELLED_VIEW = 13;
var UPDATE_CARD_ACTION = 101, ADD_CREDIT_CARD_ACTION = 102, CREATE_SUBCSRIPTION_ACTION = 103;
var PENDING_MAX_TRIES = 10;
var SLEEP_BETWEEN_PENDING_TRIES_MS = 1000;

// ViewModel
function AppViewModel() {
  var self = this;

  // Fields
  var userID;
  var pendingTries = 0;

  // Properties
  this.activeView = ko.observable(DEFAULT_VIEW);
  this.errorMessage = ko.observable("");
  this.userMessage = ko.observable("");
  this.activeView = ko.observable("");
  this.username = ko.observable("");
  this.userPassword = ko.observable("");
  this.cardBrand = ko.observable("");
  this.cardNumber = ko.observable("");
  this.cardExpirationMonth = ko.observable("");
  this.cardExpirationYear = ko.observable("");
  this.cardCVD = ko.observable("");
  this.submitCreditCardText = ko.observable("");
  this.submitCreditCardAction = ko.observable(null);

  // Constructor
  function constructor() {
    // Look for user id in query string
    if (QueryString.id) {
      self.activeView(LOADING_VIEW);
      // Check if enabled

      userID = QueryString.id.replace("{", "").replace("}", "");
      checkIfAutomaticCreditCardPaymentIsEnabled(userID);

    } else {
      // Show login
      self.activeView(LOGIN_VIEW);
    }
  }

  // Private functions
  function makeRequestToServer(methodType, path, postData, callback) {
    callback = (callback) ? callback : function() {};

    // Ajax options
    var ajaxOptions = {
        type: methodType,
        url: financeWebserviceUrl + path,
        dataType: "json",
        error: function(jqXHR, textStatus, errorThrown) {
          var errorMsg = "Server error: " + errorThrown;
          self.errorMessage(errorMsg);
          callback({message: errorMsg}, null);
        },
        success: function(data, textStatus, jqXHR) {
          if (data.error) {
            if (data.error.message) {
             self.errorMessage(data.error.message);

            } else {
              self.errorMessage(data.error);
            }
            callback(data.error, null);

          } else {
            self.errorMessage("");
            if (data.success_data) {
               callback(null, data.success_data);

            } else {
               callback(null, data);
            }
          }
        }
      };
    if (postData) {
      ajaxOptions.data = postData;
    }

    // Make request
    $.ajax(ajaxOptions);
  }

  var checkIfAutomaticCreditCardPaymentIsEnabled = function(userID) {
    var path = "api/users/" + userID +"/automatic_credit_card_payment_enabled"
    var postData = {};
    makeRequestToServer("GET", path, postData, function(error, successData) {
      if (!error) {
        if (successData.is_enabled === true) {

          // Automatic credit card is enabled -> Look for subscription
          getAndShowQuickPayInformation(userID);

        } else {
          // Automatic credit card is not enabled -> Show view
          self.activeView(AUTOMATIC_CREDIT_CARD_PAYMENT_NOT_ENABLED_VIEW);
        }
      }
    });
  };

  var enableAutomaticCreditCardPaymentOnServer = function(userID) {
    var path = "api/users/" + userID +"/automatic_credit_card_payment_enabled"
    var postData = {enabled: true};
    makeRequestToServer("POST", path, postData, function(error, successData) {
      if (!error) {
        // Make sure by checking again
        checkIfAutomaticCreditCardPaymentIsEnabled(userID);
      }
    });
  };

  var disableAutomaticCreditCardPaymentOnServer = function(userID) {
    var path = "api/users/" + userID +"/automatic_credit_card_payment_enabled"
    var postData = {enabled: false};
    makeRequestToServer("POST", path, postData, function(error, successData) {
      if (!error) {
        // Make sure by checking again
        checkIfAutomaticCreditCardPaymentIsEnabled(userID);
      }
    });
  };

  var getAndShowQuickPayInformation = function(userID) {
    makeRequestToServer("GET", "api/users/" + userID +"/quickpay_subscription", null, function(error, successData) {
      if (!error) {
        if (successData.subscription && successData.subscription.id && successData.subscription.state) {

          // Found subscription -> Check state
          switch(successData.subscription.state) {
            case "pending":
              if (pendingTries >= PENDING_MAX_TRIES) {
                self.userMessage("Kunne ikke hente kreditkort status. Prøv igen senere.");
                pendingTries = 0;

              } else {
                // Try again in a little while
                pendingTries++;
                setTimeout(function() {
                  getAndShowQuickPayInformation(userID);
                }, SLEEP_BETWEEN_PENDING_TRIES_MS);
              }
              break;
            case "cancelled":
              // Subscription cancelled
              self.activeView(SUBSCRIPTION_CANCELLED_VIEW);
              break;
            case "rejected":
              // Rejected card -> Get existing and show view
              getExistingCardInfo(userID, function() {
                self.activeView(UPDATE_REJECTED_CARD_VIEW);
              });
              break;
            case "expired":
              // Expired card -> Get existing and show view
              getExistingCardInfo(userID, function() {
                self.activeView(UPDATE_EXPIRED_CARD_VIEW);
              });
              break;
            case "initial":
                // No credit card -> Show view
                self.activeView(NO_CREDIT_CARD_VIEW);
                break;
            case "active":
                // Has creditcard -> Get creditcard
                getExistingCardInfo(userID, function() {
                  self.activeView(CREDIT_CARD_INFO_VIEW);
                });
                break;
          }

        } else {
          // No subscription -> Show view
          self.activeView(NO_SUBSCRIPTION_VIEW);
        }

      } else {
        // Check error message for no subscription found
        if (error.message && error.message.toLowerCase().indexOf("no quickpay subscription found") !== -1) {
          // No subscription found with that id -> Show as cancelled subscription forcing overwrite of existing id
          self.errorMessage("");
          self.activeView(SUBSCRIPTION_CANCELLED_VIEW);
        }
      }
    });
  }

  var getExistingCardInfo = function(userID, successCallback) {
    makeRequestToServer("GET", "api/users/" + userID + "/quickpay_subscription/credit_card", null, function(error, successData) {
      if (!error) {
        if (successData.credit_card && successData.credit_card.card_number) {

          // Found credit card -> Show info
          self.cardBrand(successData.credit_card.card_brand);
          self.cardNumber(successData.credit_card.card_number);
          self.cardExpirationMonth(successData.credit_card.card_expiration_month);
          self.cardExpirationYear(successData.credit_card.card_expiration_year);
          self.cardCVD("");
          self.submitCreditCardText("");
          self.submitCreditCardAction(null);

          // Callback
          successCallback();

        } else {
          // No credit card found -> Show view
          self.activeView(NO_CREDIT_CARD_VIEW);
        }
      }
    });
  }

  var createSubscription = function(userID, callback) {
    var path = "api/users/" + userID + "/quickpay_subscription"
    var postData = {};
    makeRequestToServer("PUT", path, postData, callback);
  };

  var createCreditCard = function(userID, callback) {
    var path = "api/users/" + userID + "/quickpay_subscription/credit_card"
    var postData = {card_number: self.cardNumber(), card_expiration_month: self.cardExpirationMonth(),
        card_expiration_year: self.cardExpirationYear(), card_cvv: self.cardCVD()};
    makeRequestToServer("PUT", path, postData, callback);
  };

  var updateCreditCard = function(userID, callback) {
    var path = "api/users/" + userID + "/quickpay_subscription/credit_card"
    var postData = {card_number: self.cardNumber(), card_expiration_month: self.cardExpirationMonth(),
        card_expiration_year: self.cardExpirationYear(), card_cvv: self.cardCVD()};
    makeRequestToServer("POST", path, postData, callback);
  };

  this.retry = function() {
    constructor();
  };

  // Public functions
  this.login = function() {
    //TODO
  };

  this.refresh = function() {
    if (location) {
      location.reload();
    }
  };

  this.enableAutomaticCreditCardPayment = function() {
    bootbox.confirm("Er du sikker på at du vil tilmelde kunden automatisk kortbetaling?", function(result) {
      if (result) {
        enableAutomaticCreditCardPaymentOnServer(userID);
      }
    });
  };

  this.disableAutomaticCreditCardPayment = function() {
    bootbox.confirm("Er du sikker på at du vil framelde kunden automatisk kortbetaling. Alt kortinformation bliver slettet! Vil du forsætte?", function(result) {
      if (result) {
        // Check for any subscription
        makeRequestToServer("GET", "api/users/" + userID +"/quickpay_subscription", null, function(error, successData) {
          if (!error) {
            if (successData.subscription && successData.subscription.id) {
              // Delete subscription
              makeRequestToServer("DELETE", "api/users/" + userID +"/quickpay_subscription", null, function(error, successData) {
                if (!error) {
                  // Subscription deleted -> Disable automatic credit card payment
                  disableAutomaticCreditCardPaymentOnServer(userID);
                }
              });

            } else {
              // No subscription
              disableAutomaticCreditCardPaymentOnServer(userID);
            }
          }
        });
      }
    });
  };

  this.showUpdateCardView = function() {
    self.cardBrand("");
    self.cardNumber("");
    self.cardExpirationMonth("");
    self.cardExpirationYear("");
    self.cardCVD("");
    self.submitCreditCardText("Opdatér");
    self.submitCreditCardAction(UPDATE_CARD_ACTION);
  	self.activeView(UPDATE_CARD_VIEW);
  };

  this.showCreateQuickPaySubscriptionView = function() {
    self.cardBrand("");
    self.cardNumber("");
    self.cardExpirationMonth("");
    self.cardExpirationYear("");
    self.cardCVD("");
    self.submitCreditCardText("Tilføj");
    self.submitCreditCardAction(CREATE_SUBCSRIPTION_ACTION);
    self.activeView(CREATE_SUBSCRIPTION_VIEW);
  };

  this.showAddCreditCardToSubscriptionView = function() {
    self.cardBrand("");
    self.cardNumber("");
    self.cardExpirationMonth("");
    self.cardExpirationYear("");
    self.cardCVD("");
    self.submitCreditCardText("Tilføj");
    self.submitCreditCardAction(ADD_CREDIT_CARD_ACTION);
    self.activeView(ADD_CREDIT_CARD_VIEW);
  };

  this.submitCreditCard = function() {
    // Set loading
    self.activeView(LOADING_VIEW);

    // Get user id
    if (QueryString.id) {
      var userID = QueryString.id.replace("{", "").replace("}", "");

      // Base functionality on action
      switch(self.submitCreditCardAction()) {
        case CREATE_SUBCSRIPTION_ACTION:
            // Create subscription
            createSubscription(userID, function(error, successData) {
              if (!error) {
                // Create credit card
                createCreditCard(userID, function(error, successData) {
                  if (!error) {
                    self.userMessage("Kreditkort oprettet");
                    getAndShowQuickPayInformation(userID);
                  }
                });
              }
            });
            break;
        case ADD_CREDIT_CARD_ACTION:
          // Create
          createCreditCard(userID, function(error, successData) {
            if (!error) {
              self.userMessage("Kreditkort oprettet");
              getAndShowQuickPayInformation(userID);
            }
          });
          break;
        case UPDATE_CARD_ACTION:
          // Update
          updateCreditCard(userID, function(error, successData) {
            if (!error) {
              self.userMessage("Kreditkort opdateret");
            }
          });
          break;
      }

    } else {
      self.errorMessage("Mangler bruger id");
    }
  };

  this.removeOldSubscriptionAndShowAddNewCard = function() {
    // Delete old subscription
    makeRequestToServer("DELETE", "api/users/" + userID +"/quickpay_subscription", null, function(error, successData) {
      if (!error) {
        // Subscription deleted -> Show add new subscription view
        self.showCreateQuickPaySubscriptionView();
      }
    });
  };

  // Call constructor
  constructor();
}
// Activate knockout
ko.applyBindings(new AppViewModel());
